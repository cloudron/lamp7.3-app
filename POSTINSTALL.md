This app is setup to use a MySQL database, redis cache and ability to send emails.

You can upload and view files using either <a href="/#/app/$CLOUDRON-APP-ID/access">SFTP</a>
or using the <a href="/#/app/$CLOUDRON-APP-ID/console">File Manager</a>.

The database credentials are stored in `credentials.txt`. phpMyAdmin access is stored in `phpmyadmin_login.txt`.

SFTP Access:

**Host**: $CLOUDRON-API-DOMAIN<br/>
**Port**: 222<br/>
**Username**: $CLOUDRON-USERNAME@$CLOUDRON-APP-DOMAIN<br/>
**Password**: Your Cloudron Password<br/>

